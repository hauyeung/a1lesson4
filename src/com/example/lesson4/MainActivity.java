package com.example.lesson4;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {

	Button startbutton;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view);
		startbutton = (Button) findViewById(R.id.startbutton);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void startclick(View view)
	{
		String startbuttontext = startbutton.getText().toString();
		if (startbuttontext.equals("Start"))
		{
			startbutton.setText("Stop");
		}
		else
		{
			startbutton.setText("Start");
		}
	}
	
	public void quitclick(View view)
	{
		finish();
	}

}
